from .server import NodeService
from .task_executor import TaskExecutor

from argparse import ArgumentParser
import json
import Pyro4


def main():
    p = ArgumentParser(description="Cluster node")
    p.add_argument(
        "--config",
        help="Path to config.json file",
        default="config.json")
    args = p.parse_args()

    with open(args.config) as config_file:
        config = json.load(config_file)

    Pyro4.config.SERIALIZER = config["serializer"]
    Pyro4.config.SERIALIZERS_ACCEPTED.add(config["serializer"])

    # all those long variable name succs
    # because long variable names are long variable names.
    node_service = NodeService(config)

    daemon = Pyro4.Daemon(host=config["host"], port=config["port"])
    node_uri = daemon.register(node_service)

    print("Node URI", node_uri)

    master_client = Pyro4.Proxy(config["master_uri"])
    master_client.connect(node_uri)

    TaskExecutor(master_client, config).task_loop()

    daemon.requestLoop()


if __name__ == "__main__":
    main()
